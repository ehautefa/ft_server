if [ "$AUTO_INDEX" = "off" ]
    then
        sed -i "s/autoindex on/autoindex off/g" ../../etc/nginx/sites-available/0wordpress
fi
if [ "$AUTO_INDEX" = "on" ]
    then
        sed -i "s/autoindex off/autoindex on/g" ../../etc/nginx/sites-available/0wordpress
fi