#!/bin/bash

../../etc/init.d/nginx start
../../etc/init.d/mysql start
../../etc/init.d/php7.3-fpm start 

sh /usr/local/bin/auto_index.sh

echo "CREATE DATABASE $DB_NAME DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;" | mariadb -u root
echo "GRANT ALL ON $DB_NAME.* TO '$DB_USER' IDENTIFIED BY '$DB_PASSWORD' WITH GRANT OPTION;" | mariadb -u root 
echo "FLUSH PRIVILEGES;" | mariadb -u root  
mysql -u root $DB_NAME < tmp/wordpress.sql

wp config create --dbname=$DB_NAME --dbuser=$DB_USER --dbpass=$DB_PASSWORD --locale=ro_RO --allow-root --path="./tmp/wordpress"
chmod 644 ./tmp/wordpress/wp-config.php
passgen=`head -c 10 /dev/random | base64`
password=${passgen:0:10}
wp core install --allow-root --path="./tmp/wordpress" --url=localhost --title="Miette" --admin_name=$DB_UDSER --admin_password=$password  --admin_email=ehautefa@student.42.fr
cp -a /tmp/wordpress/. /var/www/wordpress
chown -R www-data:www-data /var/www/wordpress

tail -f var/log/nginx/error.log var/log/nginx/access.log