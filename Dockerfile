FROM debian:buster

LABEL maintainer="ehautefa@student.42.fr"

RUN apt update -y \
    && apt upgrade -y \
    && apt install -y nginx \
	&& apt install -y php-mysql php-fpm \
	&& apt install -y php-curl php-gd php-intl php-mbstring php-soap php-xml php-xmlrpc php-zip \
	&& apt install -y mariadb-server \
	&& apt install -y vim \
    && apt clean -y
 
EXPOSE 80

ENV DB_NAME 'wordpress'
ENV DB_USER  'ehautefa'
ENV DB_PASSWORD  'pass'
ENV AUTO_INDEX 'on'

# config nginx
RUN mkdir /var/www/wordpress \
	&& chown -R $USER:$USER /var/www/wordpress
COPY ./srcs/nginx.conf /etc/nginx/sites-available/0wordpress
RUN ln -s /etc/nginx/sites-available/0wordpress /etc/nginx/sites-enabled/



#auto index 
ADD ./srcs/auto_index.sh /usr/local/bin/auto_index.sh
RUN chmod 755 /usr/local/bin/auto_index.sh

#install wp-cli
COPY ./srcs/wp-cli.phar /usr/local/bin/wp
RUN chmod +x /usr/local/bin/wp

#Add wordpress
ADD ./srcs/wordpress ./tmp/wordpress
ADD ./srcs/wordpress.sql ./tmp/wordpress.sql

#Add phpmyadmin
RUN mkdir /var/www/wordpress/phpmyadmin 
COPY ./srcs/phpmyadmin.conf /etc/nginx/sites-available/phpmyadmin
RUN ln -s /etc/nginx/sites-available/phpmyadmin /etc/nginx/sites-enabled/
ADD ./srcs/phpmyadmin ./var/www/wordpress/phpmyadmin
RUN chown -R www-data:www-data /var/www/wordpress/phpmyadmin

#certificat ssl
RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
	-keyout /etc/ssl/private/nginx-selfsigned.key \
	-out /etc/ssl/certs/nginx-selfsigned.csr \
	-subj '/CN=localhost'

#create sql database
ADD ./srcs/create_database.sh /usr/local/bin/create_database.sh
RUN chmod 755 /usr/local/bin/create_database.sh

CMD ["./usr/local/bin/create_database.sh"]

# TO BUILD : docker build -t ehautefa/server .
# TO RUN :  docker run --rm -it -p 80:80 -e AUTO_INDEX='off' ehautefa/server